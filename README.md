## Descripción

Este es el frontend realizado para la interacción con la autenticación y el sistema de administración de tareas.

## Variables de entorno

Es muy importante que cree un archivo llamado `.env`, el cual lo puede crear a partir del archivo llamado `.copy.env` en la raiz aqui por favor ponga la url del servicio de backend en una variable de entorno llamada API_URL.

## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm run start
```

## Build the app

```bash
$ npm run build
```

## Test

```bash
# unit tests
$ npm run test
```

Con esto debe ser capaz de poder ejecutar la aplicación y empezarla a usar.