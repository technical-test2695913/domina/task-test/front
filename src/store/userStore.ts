// Store
import { Store, store } from "./store";

// Types
import { IUser } from "../shared/interfaces/IUser";
import { ILogin } from "../shared/interfaces/ILogin";
import { IChangePass, IRequestChangePass } from "../shared/interfaces/IRequestChangePass";

class UserStore {
  store?: Store = undefined;

  constructor(store: Store) {
    this.store = store;
  }

  getUserData() {
    return this.store?.get(`/user/${this.store?.userId}`);
  }

  saveUser(user: IUser) {
    return this.store?.post('/user', user);
  }

  editUser(user: IUser) {
    console.log('oooooo', user)
    return this.store?.put(
      `/user/${this.store?.userId}`,
      {...user, id: this.store?.userId});
  }

  verify(token: string) {
    return this.store?.post(`/user/verify/${token}`);
  }

  login(data: ILogin) {
    return this.store?.post('/auth/login', data)
  }

  requestChangePass(data: IRequestChangePass) {
    return this.store?.post('/auth/change-pass-apply', data);
  }

  changePass(data: IChangePass) {
    return this.store?.post('/auth/change-pass', data);
  }
}

export const userStore = new UserStore(store);
