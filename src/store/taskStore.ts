// Store
import { Store, store } from "./store";

// Types
import { ITask } from "../shared/interfaces/ITask";

class TaskStore {
  store?: Store = undefined;

  constructor(store: Store) {
    this.store = store;
  }

  create(task: ITask) {
    return this.store?.post('/task', {
      ...task,
      lang: this.store.lang || 'en',
      user: this.store.userId
    });
  }

  async getAll() {
    return await this.store?.get('/task');
  }

  edit(task: ITask) {
    return this.store?.put(`/task/${task.id}`, task);
  }

  deleteTask(id: number) {
    return this.store?.delete(`/task/${id}`);
  }
}

export const taskStore = new TaskStore(store);
