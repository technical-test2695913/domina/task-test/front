import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";
import { Country } from "country-state-city";
import env from "react-dotenv";

// Types
import { ISelector } from "../shared/interfaces/ISelector";

export class Store {
  token: string | null = null;
  userId: number | null = null;
  userEmail: string | null = null;
  genders: ISelector[] = [];
  countries: ISelector[] = [];
  loading = false;
  lang = 'en';

  constructor() {
    makeAutoObservable(this);
    makePersistable(this, {
      name: 'Store',
      properties: ['token', 'userId', 'userEmail', 'genders', 'countries', 'lang'],
      storage: window.localStorage
    })

    if (this.genders.length === 0) this.setGenders()
    if (this.countries.length === 0) this.countries = Country
      .getAllCountries().map(({ name, isoCode }) =>
        ({ id: isoCode, code: `${isoCode}, ${name}` }))
  }

  setLang(lang: string) {
    this.lang = lang;
  }

  setGenders() {
    this.genders = ['M', 'F', 'O'].map(item => ({id: item, code: item}));
  }

  setToken(token: string | null) {
    this.token = token;
  }

  setUserId(id: number | null) {
    this.userId = id;
  }

  setUserEmail(email: string | null) {
    this.userEmail = email;
  }

  setLoading(loading: boolean) {
    this.loading = loading;
  }

  get isAuthenticated() {
    return Boolean(this.token);
  }

  private get headers() {
    var hds = new Headers();
    hds.append("accept", "*/*");
    hds.append("Cookie", "cf_ob_info=504:7b12b307ff65f78a:BOG; cf_use_ob=443")
    hds.append("Content-Type", "application/json")
    if (this.token) hds.append("Authorization", `Bearer ${this.token}`);

    return hds;
  }

  private async apiMiddleware(
    url: string,
    init?: RequestInit | undefined
  ): Promise<any> {
    try {
      this.setLoading(true);
      const response = await fetch(`${env.API_URL}${url}`, init);
      const result = await response.json();
      this.setLoading(false);
      return result?.data || result;
    } catch (error) {
      console.log(error);
    }
  }

  get(url: string) {
    return this.apiMiddleware(url, {
      method: 'GET',
      headers: this.headers
    })
  }

  post(url: string, body: any = {}) {
    return this.apiMiddleware(url, {
      method: 'POST',
      headers: this.headers,
      body: JSON.stringify(body)
    })
  }

  put(url: string, body: any = {}) {
    return this.apiMiddleware(url, {
      method: 'PUT',
      headers: this.headers,
      body: JSON.stringify(body)
    })
  }

  delete(url: string) {
    return this.apiMiddleware(url, {
      method: 'DELETE',
      headers: this.headers
    })
  }
}

export const store = new Store();
