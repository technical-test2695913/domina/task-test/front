import { useEffect, useRef, useState } from 'react';
import { observer } from 'mobx-react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

// Layout
import { MainLayout } from '../../layouts/Main';

// Store
import { store } from '../../store';
import { taskStore } from '../../store/taskStore';

// Components
import { Input } from '../../components/Input';

// Types
import { ITask } from '../../shared/interfaces/ITask';

import './styles.scss';

export const Tasks = observer(() => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [text, setText] = useState('')
  const [tasks, setTasks] = useState<ITask[]>([])
  const [selectedTask, setSelectedTask] = useState<ITask>()
  const ref = useRef(null)

  const setTaskOfList = (task: ITask, remove = false) => {
    const i = tasks.findIndex(t => t.id === task.id)
    if (remove) setTasks([...tasks.slice(0, i), ...tasks.slice(i + 1)])
    else setTasks([...tasks.slice(0, i), task, ...tasks.slice(i + 1)])
  }

  const handleCreateTask = async () => {
    if (selectedTask) {
      const res = await taskStore.edit({ ...selectedTask, text });
      if (res === true) setTaskOfList({ ...selectedTask, text });
      setSelectedTask(undefined);
    } else {
      const res = await taskStore.create({ text });
      if (res.id) setTasks([res, ...tasks])
    }
    setText('')
  }

  const handleCheckTask = async (task: ITask) => {
    const res = await taskStore.edit({ ...task, check: !task.check })
    if (res === true) setTaskOfList({ ...task, check: !task.check })
  }

  const handleDeleteTask = async (task: ITask) => {
    if (!task.id) return;
    const res = await taskStore.deleteTask(task.id)
    if (res === true) setTaskOfList(task, true)
  }

  useEffect(() => {
    if (!store.isAuthenticated) navigate('/');
  }, [navigate])

  useEffect(() => {
    setText(selectedTask?.text || '');
    (ref?.current as any)?.focus();
  }, [selectedTask])

  useEffect(() => {
    (async () => {
      setTasks(await taskStore.getAll())
    })()
  }, [])

  return (
    <MainLayout>
      <section className='col-12'>
        <section className='d-lg-flex justify-content-center align-items-center'>
          <Input
            className='col-12 col-lg-6'
            label={t(selectedTask ? 'TASK.INPUT_UPDATE' : 'TASK.INPUT')}
            name='task'
            value={text}
            onChange={e => setText(e.target.value)}
            refInput={ref}
            touched={text.trim() !== ''}
            error={text.trim().length < 10 || text.trim().length > 500 ? t('TASK.ERROR') : ''}
          />
          <button
            className={`btn col-12 col-lg-3 ms-0 ms-lg-3 mb-3 ${selectedTask ? 'btn-warning' : 'btn-primary'}`}
            type="button"
            disabled={text.trim().length < 10 || text.trim().length > 500}
            onClick={handleCreateTask}>
            {t(selectedTask ? 'TASK.UPDATE' : 'TASK.SEND')}
          </button>
        </section>
        <section className='col-12 col-lg-6 mx-auto'>
          {tasks.map(task => (
            <div
              className="form-check border-bottom position-relative task"
              key={`ITEN-${task.id}`}>
              <input
                className="form-check-input"
                type="checkbox"
                id={`ITEN-${task.id}`}
                checked={task.check}
                onChange={() => handleCheckTask(task)} />
              <label
                className="form-check-label pe-5"
                htmlFor={`ITEN-${task.id}`}>
                {task.check ? <s>{task.text}</s> : task.text}
              </label>
              <div className='position-absolute end-0 top-0'>
                {!task.check && <i
                  className="bi bi-pencil-square me-1"
                  onClick={() => setSelectedTask(task)} />}
                <i
                  className="bi bi-trash-fill ms-1"
                  onClick={() => handleDeleteTask(task)} />
              </div>
            </div>
          ))}
        </section>
      </section>
    </MainLayout>
  )
})
