import { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { useNavigate } from 'react-router-dom';

// Layout
import { MainLayout } from '../../layouts/Main';

// Store
import { store, userStore } from '../../store';

import './styles.scss';
import { Signup } from '../../components/auth/Signup';

export const Profile = observer(() => {
  const navigate = useNavigate();
  const [data, setData] = useState<any>()

  useEffect(() => {
    if (!store.isAuthenticated) navigate('/');
  }, [navigate])

  useEffect(() => {
    (async () => {
      setData(await userStore.getUserData());
    })()
  }, [])

  return (
    <MainLayout>
      {data && <div className="home col-12 col-sm-10 col-md-6 col-lg-4">
        <Signup profile={{
          ...data,
          country: data.country || '',
          city: data.city || '',
          address: data.address || ''
        }} />
      </div>}
    </MainLayout>
  )
})
