import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Formik } from "formik";

// Types
import { IChangePass } from "./interface";

// Components
import { CardLink } from "../../components/card/CardLinks";

// Formik
import { initialValues, validationSchema } from "./form.formik";

// Layout
import { MainLayout } from "../../layouts/Main";

// Components
import { Input } from "../../components/Input";

// Store
import { userStore } from "../../store";

export const ChangePass = ({ onSignup, onLogin }: IChangePass) => {
  const { t } = useTranslation();
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const [error, setError] = useState<string>();
  const [success, setSuccess] = useState(false);
  const types: any = {
    password: "password",
    confirmPassword: "password"
  }

  const handleSubmit = async (values: any) => {
    const res = await userStore.changePass({
      ...values,
      changePassToken: searchParams.get('changePassToken')
    });
    if (res !== true) setError(res.message || 'UNEXPECTED');
    else setSuccess(true);
  }

  useEffect(() => {
    if (!searchParams.get('changePassToken')) navigate('/');
  }, [searchParams, navigate])

  return (
    <MainLayout>
      <CardLink
        title={t('CHANGE_PASS.TITLE')}
        className="col-12 col-sm-10 col-md-6 col-lg-4"
        actions={[
          { label: t('CHANGE_PASS.LOGIN'), action: () => navigate('/') }
        ]}>
        {!success
          ? <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}>
            {({ values, errors, handleSubmit, handleChange, touched }) => (
              <form
                onSubmit={handleSubmit}
                className={`p-3 row justify-content-center`}>
                {Object.keys(values).map((key: string, i) => (
                  <Input
                    key={key}
                    name={key}
                    type={types[key]}
                    className={`col-12`}
                    label={t(`SIGNUP.INPUTS.${key.toUpperCase()}`)}
                    onChange={handleChange}
                    value={(values as any)[key]}
                    error={(errors as any)[key] && t((errors as any)[key])}
                    touched={(touched as any)[key]} />
                ))}

                <div className="col-12 text-center my-3">
                  <button className="btn btn-primary" type="submit">{t('BTN.SEND')}</button>
                </div>
                {error && <div className="alert alert-danger text-center" role="alert">
                  {t(`CHANGE_PASS.${error}`)}
                </div>}
              </form>
            )}
          </Formik>
          : <h6 className="m-3"><i>{t('CHANGE_PASS.MSG_END')}</i></h6>}
      </CardLink>
    </MainLayout>
  )
}