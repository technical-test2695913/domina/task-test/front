// Components
import { LOGIN } from './components/auth/login/es'
import { SIGNUP } from './components/auth/signup/es'
import { CHANGE_PASS } from './components/auth/changePass/es'

export const es = {
  translation: {
    APP: 'Domina tus tareas',
    SELECT: 'Selecciona ...',
    LOGIN,
    SIGNUP,
    CHANGE_PASS,
    BY: 'Por',
    BTN: {
      SEND: 'Enviar',
      UPDATE: 'Actualizar',
      CANCEL: 'Cancel',
      NEXT: 'Siguiente',
      BACK: 'Anterior',
    },
    USER_CREATED: {
      TITLE: 'Usuario creado con éxito',
      MSG: '¡Bienvenido!, el usuario fue creado con éxito, por favor diríjase al correo electrónico que facilito para que pueda activar su cuenta.'
    },
    VERIFY_USER: {
      TITLE: 'Verificar usuario',
      MSG: 'Bienvenido, por favor dale click al siguiente botón para verificar su cuenta.',
      ACTION: 'Verificar'
    },
    GENDER: {
      M: 'Masculino',
      F: 'Femenino',
      O: 'Otro'
    },
    TASK: {
      INPUT: 'Nueva tarea',
      INPUT_UPDATE: 'Actualizar tarea',
      SEND: 'Enviar',
      UPDATE: 'Actualizar',
      ERROR: 'El texto de la tarea debe tener entre 10 y 500 caracteres'
    }
  }
}