export const SIGNUP = {
  TITLE: 'Crear usuario',
  CHANGE: 'Cambiar contraseña',
  LOGIN: 'Iniciar sesión',
  VAL_USER_ALREADY_REGISTERED_WITH_EMAIL: 'El usuario con el correo #EMAIL ya existe',
  INPUTS: {
    NAME: 'Nombres',
    LASTNAME: 'Apellidos',
    EMAIL: 'Email',
    PASSWORD: 'Password',
    CONFIRMPASSWORD: 'Confirma la contraseña',
    BIRTHDAY: 'Fecha de cumpleaños',
    GENDER: 'Genero',
    COUNTRY: 'País',
    CITY: 'Ciudad',
    ADDRESS: 'Direccion'
  },
  VALIDATIONS: {
    NAME: {
      REQUIRED: 'Los nombes son requeridos',
      MIN: 'Los nombes deben tener al menos 3 caracteres',
      MAX: 'Los nombes deben tener maximo 50 caracteres'
    },
    LASTNAME: {
      REQUIRED: 'Los apellidos son requeridos',
      MIN: 'Los apellidos deben tener al menos 3 caracteres',
      MAX: 'Los apellidos deben tener maximo 50 caracteres'
    },
    EMAIL: {
      REQUIRED: 'El correo es requerido',
      FORMAT: 'El formato del correo no es correcto',
      MIN: 'El correo debe tener al menos 3 caracteres',
      MAX: 'El correo debe tener maximo 255 caracteres'
    },
    PASSWORD: {
      REQUIRED: 'La contraseña es requerida',
      FORMAT: 'La contraseña debe tener al menos 8 caracteres y contener al menos una letra mayúscula, una letra minúscula, un número y un carácter especial',
      MIN: 'La contraseña debe tener al menos 8 caracteres',
      MAX: 'La contraseña debe tener maximo 128 caracteres'
    },
    BIRTHDAY: {
      REQUIRED: 'La fecha de cumpleaños es requerida',
      MAX: 'Al menos debes tener 14 años'
    },
    GENDER: {
      REQUIRED: 'El genero es requerido'
    },
    COUNTRY: {
      REQUIRED: 'El país es requerido',
      MIN: 'El país debe tener al menos 3 caracteres',
      MAX: 'El país debe tener maximo 200 caracteres'
    },
    CITY: {
      REQUIRED: 'La ciudad es requerida',
      MIN: 'La ciudad debe tener al menos 3 caracteres',
      MAX: 'La ciudad debe tener maximo 200 caracteres'
    },
    ADDRESS: {
      REQUIRED: 'La direccion es requerida',
      MIN: 'La dirección debe tener al menos 3 caracteres',
      MAX: 'La dirección debe tener maximo 500 caracteres'
    }
  }
}