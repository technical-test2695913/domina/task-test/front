export const CHANGE_PASS = {
  TITLE: 'Change Password',
  SIGNUP: 'Sign up',
  LOGIN: 'Log in',
  MSG: 'Correct! We will send you an access link to your email so you can change your password.',
  MSG_END: 'Password changed successfully!',
  VAL_TOKEN_NOT_FOUND: 'Invalid token',
  UNEXPECTED: 'Unexpected error, please try again later',
  VAL_PASS_NOT_MATCH: 'Passwords do not match',
  VAL_USER_NOT_EXIST_WITH_THIS_EMAIL: 'There is no user registered with this email',
  INPUTS: {
    CONFIRMPASSWORD: 'Confirm the password'
  },
  VALIDATIONS: {
    CONFIRMPASSWORD: {
      REQUIRED: 'The confirmation password is required',
      SAME: 'Passwords do not match',
    }
  }
}