// Components
import { LOGIN } from './components/auth/login/en'
import { SIGNUP } from './components/auth/signup/en'
import { CHANGE_PASS } from './components/auth/changePass/en'

export const en = {
  translation: {
    APP: 'Domina your tasks',
    SELECT: 'Choose ...',
    LOGIN,
    SIGNUP,
    CHANGE_PASS,
    BY: 'By',
    BTN: {
      SEND: 'Send',
      UPDATE: 'Update',
      CANCEL: 'Cancel',
      NEXT: 'Next',
      BACK: 'Back'
    },
    USER_CREATED: {
      TITLE: 'User created successfully',
      MSG: 'Welcome, the user was created successfully, please go to the email that I provided so that you can activate your account.'
    },
    VERIFY_USER: {
      TITLE: 'Verify user',
      MSG: 'Welcome, please click the button below to verify your account.',
      ACTION: 'Verify'
    },
    GENDER: {
      M: 'Male',
      F: 'Female',
      O: 'Other'
    },
    TASK: {
      INPUT: 'New task',
      INPUT_UPDATE: 'Update task',
      SEND: 'Send',
      UPDATE: 'Update',
      ERROR: 'Task text must be between 10 and 500 characters'
    }
  }
}