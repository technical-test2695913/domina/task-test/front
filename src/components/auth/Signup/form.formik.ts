import moment from "moment";
import * as Yup from "yup";

export const validationSchema = (isProfile=false) => {
  let data: any = {
    name: Yup.string()
      .required('SIGNUP.VALIDATIONS.NAME.REQUIRED')
      .min(3, 'SIGNUP.VALIDATIONS.NAME.MIN')
      .max(50, 'SIGNUP.VALIDATIONS.NAME.MAX'),
    lastName: Yup.string()
      .required('SIGNUP.VALIDATIONS.LASTNAME.REQUIRED')
      .min(3, 'SIGNUP.VALIDATIONS.LASTNAME.MIN')
      .max(50, 'SIGNUP.VALIDATIONS.LASTNAME.MAX'),
    birthDay: Yup.date()
      .required('SIGNUP.VALIDATIONS.BIRTHDAY.REQUIRED')
      .max(moment().subtract(14, 'years').toDate(), 'SIGNUP.VALIDATIONS.BIRTHDAY.MAX'),
    gender: Yup.string()
      .required('SIGNUP.VALIDATIONS.GENDER.REQUIRED'),
    country: Yup.string()
      // .required('SIGNUP.VALIDATIONS.COUNTRY.REQUIRED')
      // .min(3, 'SIGNUP.VALIDATIONS.COUNTRY.MIN')
      .max(200, 'SIGNUP.VALIDATIONS.COUNTRY.MAX'),
    city: Yup.string()
      // .required('SIGNUP.VALIDATIONS.CITY.REQUIRED')
      // .min(3, 'SIGNUP.VALIDATIONS.CITY.MIN')
      .max(200, 'SIGNUP.VALIDATIONS.CITY.MAX'),
    address: Yup.string()
      // .required('SIGNUP.VALIDATIONS.ADDRESS.REQUIRED')
      // .min(3, 'SIGNUP.VALIDATIONS.ADDRESS.MIN')
      .max(500, 'SIGNUP.VALIDATIONS.ADDRESS.MAX'),
  }
  if (!isProfile) data = {...data,
    email: Yup.string()
      .required('SIGNUP.VALIDATIONS.EMAIL.REQUIRED')
      .email('SIGNUP.VALIDATIONS.EMAIL.FORMAT')
      .min(8, 'SIGNUP.VALIDATIONS.EMAIL.MIN')
      .max(255, 'SIGNUP.VALIDATIONS.EMAIL.MAX'),
    password: Yup.string()
      .required('SIGNUP.VALIDATIONS.PASSWORD.REQUIRED')
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
        'SIGNUP.VALIDATIONS.PASSWORD.FORMAT')
      .min(3, 'SIGNUP.VALIDATIONS.PASSWORD.MIN')
      .max(50, 'SIGNUP.VALIDATIONS.PASSWORD.MAX'),
    }
  return Yup.object(data);
}

export const initialValues = {
  name: '',
  lastName: '',
  birthDay: '',
  gender: '',
  country: '',
  city: '',
  address: '',
  email: '',
  password: '',
};

export const steps = [
  [0, 2],
  [2, 4],
  [4, 7],
  [7, 9]
]
