import { useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { Formik } from "formik";
import { observer } from "mobx-react";
import { City, State } from "country-state-city";
import { useNavigate } from "react-router-dom";

// Types
import { ISignup } from "./interface"
import { ISelector } from "../../../shared/interfaces/ISelector";

// Components
import { CardLink } from "../../card/CardLinks";

// Formik
import { initialValues, steps, validationSchema } from "./form.formik";

// Store
import { store, userStore } from "../../../store"

import './styles.scss';
import { Steps } from "../../Steps";

export const Signup = observer(({ onChangePassword, onLogin, profile }: ISignup) => {
  const { t, i18n: { language } } = useTranslation()
  const navigate = useNavigate();
  const [error, setError] = useState<string>()
  const types: any = {
    password: 'password',
    birthDay: 'date',
    gender: 'select',
    country: 'autocomplete',
    city: 'autocomplete'
  }

  const logout = () => {
    store.setToken(null);
    navigate('/');
  }

  const handleSubmit = async (values: any) => {
    console.log('eeeee', values)
    const res = profile
      ? await userStore.editUser(values)
      : await userStore.saveUser({
        ...values,
        lang: language
      });;
    if (res.message) setError(res.message);
    else if (profile) logout();
    else navigate("/user-created", { state: res });
  }

  const getCities = (country: string) => {
    country = country.split(',')[0];
    return City.getCitiesOfCountry(country)
      ?.map(({ name, stateCode }, i) => {
        const state = State.getStateByCodeAndCountry(stateCode, country)?.name;
        const city = `${name} (${state})`
        return ({ id: city, code: city });
      }) || []
  }

  const getInitialValues = () => {
    const values = {
      name: (profile || initialValues).name,
      lastName: (profile || initialValues).lastName,
      birthDay: (profile || initialValues).birthDay,
      gender: (profile || initialValues).gender,
      country: (profile || initialValues).country,
      city: (profile || initialValues).city,
      address: (profile || initialValues).address,
      email: (profile || initialValues).email,
      password: (profile || initialValues).password,
    }
    if (profile) {
      delete values.email;
      delete values.password;
    }
    return values;
  }

  return (
    <CardLink
      title={t('SIGNUP.TITLE')}
      actions={[
        { label: t('SIGNUP.CHANGE'), action: onChangePassword },
        { label: t('SIGNUP.LOGIN'), action: onLogin }
      ].filter(item => item.action)}>
      <Formik
        initialValues={getInitialValues()}
        validationSchema={validationSchema(Boolean(profile))}
        onSubmit={handleSubmit}>
        {({ values, errors, handleSubmit, handleChange, touched, validateField }) => {
          const [cities, setCities] = useState<ISelector[]>([])
          const selectors: any = {
            gender: store.genders.map(g => ({ ...g, code: t(`GENDER.${g.code}`) })),
            country: store.countries,
            city: cities
          }

          useMemo(() => setCities(getCities(values.country)), [values.country])

          return (
            <Steps
              aliasTranslation="SIGNUP"
              error={error}
              errors={errors}
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              validateField={validateField}
              selectors={selectors}
              steps={profile ? steps.slice(0, steps.length - 1) : steps}
              touched={touched}
              types={types}
              values={values} />
          )
        }}
      </Formik>
    </CardLink>
  )
})