import { useTranslation } from "react-i18next";

import "./styles.scss";
import { useEffect } from "react";
import { store } from "../../store";

export const Langs = () => {
  const { i18n: { language, changeLanguage } } = useTranslation()

  const handleChangeLanguage = (lang: string) => {
    store.setLang(lang)
    changeLanguage(lang)
  }

  useEffect(() => {
    changeLanguage(store.lang)
  }, [changeLanguage])

  return (
    <article className="langs">
      <section
        className={language === 'es' ? 'active' : ''}
        onClick={() => handleChangeLanguage('es')}>ES</section>
      <section
        className={language === 'en' ? 'active' : ''}
        onClick={() => handleChangeLanguage('en')}>EN</section>
    </article>
  )
}