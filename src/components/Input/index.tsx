import { useTranslation } from "react-i18next";
import { Field } from "formik";

// Types
import { IInput } from "./interface";

import './styles.scss';
import { useRef } from "react";

export const Input = ({
  type = 'text',
  value,
  name,
  id,
  label,
  className,
  onChange,
  setFieldValue,
  error,
  touched,
  options = [],
  refInput
}: IInput) => {
  const { t, i18n: { language } } = useTranslation();
  const refFile = useRef(null);

  const inputClassName = touched && error
    ? 'is-invalid'
    : (touched ? 'border-success text-success' : '');
  const labelClassName = touched && error
    ? 'text-danger'
    : (touched ? 'text-success' : '');

  const onChangeFile = (e: any) => {
    const file = e.target.files[0];
    if (!file) return;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      if (setFieldValue && name) setFieldValue(name, reader.result);
    };
  }

  return (
    <div className={`form-floating mb-3 ${className}`}>
      {type === 'select'
        ? <Field
          as="select"
          value={value}
          className={`form-select ${inputClassName}`}
          name={name}
          id={id || name}
          placeholder={name}>
          <option value=''>{t('SELECT')}</option>
          {options.map(opt => (
            <option key={opt.id} value={opt.id}>
              {(opt as any)[language] || opt.code}
            </option>
          ))}
        </Field>
        : (type === 'file'
          ? <div
            className="border input__file"
            onClick={() => ((refInput || refFile).current as any).click()}>
            <article>
              {Boolean(value) && <img src={value} alt={label || ''} />}
            </article>
            <p className="text-truncate">{value.replace('data:image/jpeg;base64,/9j/', '') || label}</p>
            <input type='file' className="d-none" ref={(refInput || refFile)} onChange={onChangeFile} />
          </div>
          : <input
            type={type === 'autocomplete' ? 'text' : type}
            value={value}
            className={`form-control ${inputClassName}`}
            name={name}
            id={id || name}
            placeholder={name}
            onChange={onChange}
            list={`list-${name}`}
            ref={refInput} />
        )
      }
      <datalist id={`list-${name}`}>
        {options.map(opt => (
          <option key={opt.id} value={opt.code} />
        ))}
      </datalist>
      <label htmlFor={id || name} className={`ps-3 ${labelClassName} ${type === 'file' && 'd-none'}`}>
        {label}
      </label>
      {error && touched && <div className="signup--error">
        {error}
      </div>}
    </div>
  )
}
