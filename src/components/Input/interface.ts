import { ChangeEventHandler, HTMLInputTypeAttribute, LegacyRef } from "react";

// Types
import { ISelector } from "../../shared/interfaces/ISelector";

export interface IInput {
  name?: string;
  id?: string;
  value?: any;
  type?: HTMLInputTypeAttribute | 'select' | 'autocomplete';
  label?: string | null;
  className?: string;
  onChange?: ChangeEventHandler<HTMLInputElement> | undefined
  setFieldValue?: (field: string, value: any, shouldValidate?: boolean | undefined) => void
  error?: string | null;
  touched?: boolean;
  options?: ISelector[]
  refInput?: any
}