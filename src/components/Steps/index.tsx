import React, { useCallback, useEffect, useState } from 'react'
import { ISteps } from './interface'
import { Input } from '../Input'
import { useTranslation } from 'react-i18next'

export const Steps = ({
  handleSubmit,
  steps = [],
  types = {},
  values = {},
  errors = {},
  touched = {},
  selectors = {},
  aliasTranslation = '',
  error,
  validateField,
  handleChange
}: ISteps) => {
  const { t } = useTranslation()
  const [currentStep, setCurrentStep] = useState(0)
  const [currentTouched, setCurrentTouched] = useState<any>({})
  const keys = Object.keys(values)

  const nextStep = () => {
    if (currentStep === steps.length - 1) return

    const [start, end] = steps[currentStep]
    let newTouched = {}

    for (let index = start; index < end; index++) {
      if (validateField) validateField(keys[index])
      newTouched = { ...newTouched, [keys[index]]: true }
    }
    setCurrentTouched({ ...currentTouched, ...newTouched })
    setCurrentStep(currentStep + 1)
  }

  const canNext = useCallback((step: number) => {
    let canNext = Boolean(steps[step])
    if (!canNext) return false

    const [start, end] = steps[step]
    for (let index = start; index < end; index++) {
      canNext &&= !Boolean(errors[keys[index]])
    }
    return canNext
  }, [errors, keys, steps])

  useEffect(() => {
    setCurrentTouched(touched)
  }, [touched])

  useEffect(() => {
    if (currentStep > 0 && !canNext(currentStep - 1)) {
      setCurrentStep(currentStep - 1)
    }
  }, [currentStep, canNext])

  return (
    <form
      onSubmit={handleSubmit}
      className={`p-3 row justify-content-center`}>
      {keys.slice(...steps[currentStep]).map((key: string, i) => (
        <Input
          key={key}
          name={key}
          type={types[key] || 'text'}
          className='col-12'
          label={t(`${aliasTranslation}.INPUTS.${key.toUpperCase()}`)}
          onChange={handleChange}
          value={values[key]}
          error={errors[key] && t(errors[key])}
          touched={currentTouched[key]}
          options={selectors[key]} />
      ))}

      <div className="col-12 text-center my-3">
        {currentStep > 0 &&
          <button
            className="btn btn-warning me-3"
            type="button"
            onClick={() => setCurrentStep(currentStep - 1)}>
            {t('BTN.BACK')}
          </button>}
        {currentStep < steps.length - 1 &&
          <button
            className="btn btn-primary"
            type="button"
            onClick={nextStep}>
            {t('BTN.NEXT')}
          </button>}
        {currentStep === steps.length - 1 &&
          <button
            className="btn btn-primary"
            type="submit">
            {t('BTN.SEND')}
          </button>}
      </div>
      {error && <div className="alert alert-danger text-center" role="alert">
        {t(`${aliasTranslation}.${error}`).replace('#EMAIL', values.email)}
      </div>}
    </form>
  )
}
