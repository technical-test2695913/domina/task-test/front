import { ChangeEvent } from "react";

export interface ISteps {
  handleSubmit?: (values: any) => void;
  steps?: number[][]
  types?: any
  values?: any
  errors?: any
  touched?: any
  selectors?: any
  aliasTranslation?: string
  error?: string
  validateField?: (field: string) => void
  handleChange?: {
    (e: ChangeEvent<any>): void;
    <T = string | ChangeEvent<any>>(field: T): T extends ChangeEvent<any> ? void : (e: string | ChangeEvent<any>) => void;
}
}