// Types
import { ICardLink } from "./interface"

import './styles.scss'

export const CardLink = ({ title, children, actions, className }: ICardLink) => {
  return (
    <article className={`card_links shadow ${className}`}>
      <h2 className="card_links__title">
        {title}
      </h2>
      {children}
      {actions && actions.length > 0 && <hr />}
      {(actions || []).map(({ label, action }, i) => (
        <p key={`CARD-LINK${i}`} onClick={action} className="card_links__action">
          {label}
        </p>
      ))}
    </article>
  )
}