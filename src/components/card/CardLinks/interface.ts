export interface ICardLink {
  title: string;
  children?: any;
  actions?: ICardLinkAction[]
  className?: string;
}

export interface ICardLinkAction {
  label: string;
  action?: () => void;
}
