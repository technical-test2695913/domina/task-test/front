export interface ITask {
  id?: number
  text?: string
  user?: number
  check?: boolean
  active?: boolean
  createdAt?: string
  updatedAt?: string
  lang?: string
}