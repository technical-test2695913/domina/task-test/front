export interface ISelector {
  id?: number | string;
  code?: string;
}
