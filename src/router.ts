import { createBrowserRouter } from "react-router-dom";

// Pages
import { Home } from "./pages/Home";
import { Tasks } from "./pages/Tasks";
import { UserCreated } from "./pages/UserCreated";
import { VerifyUser } from "./pages/VerifyUser";
import { ChangePass } from "./pages/ChangePass";
import { Profile } from "./pages/profile";

export const router = createBrowserRouter([
  {
    path: "/",
    Component: Home
  },
  {
    path: "/tasks",
    Component: Tasks,
  },
  {
    path: "/profile",
    Component: Profile,
  },
  {
    path: "/user-created",
    Component: UserCreated,
  },
  {
    path: "/verify-user",
    Component: VerifyUser,
  },
  {
    path: "/change-pass",
    Component: ChangePass,
  },
]);