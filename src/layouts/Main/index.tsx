import { useTranslation } from 'react-i18next';
import { NavLink, useNavigate } from 'react-router-dom';

// Types
import { IMainLayout } from './interface';

// Components
import { Langs } from '../../components/Langs';
import { Loading } from '../../components/Loading';

// Store
import { store } from '../../store';

import './styles.scss'

export const MainLayout = ({ children, className }: IMainLayout) => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const handleLogout = () => {
    store.setToken(null);
    navigate('/');
  }

  return (
    <div className='main_layout'>
      <nav>
        <NavLink to='/'>
          <h1>{t('APP')}</h1>
        </NavLink>
        <section className='d-flex flex-row gap-3 align-items-center'>
          {store.isAuthenticated && <>
            <i className='text-light d-none d-lg-block'>{store.userEmail}</i>
            <i
              className="fs-2 bi bi-person-fill-gear text-light main_layout__logout"
              onClick={() => navigate('/profile')} />
          </>}
          <div className='d-none d-lg-block'>
            <Langs />
          </div>
          {store.isAuthenticated && <i
            className="fs-2 bi bi-box-arrow-left text-light main_layout__logout"
            onClick={handleLogout} />}
        </section>
      </nav>
      <main className={className}>
        {children}
      </main>
      <Loading />
    </div>
  )
}
